# NvChad Customizations

https://nvchad.com/

This directory contains my customizations for my nvim configuration on top of
NvChad.

After installing NvChad, this directory should be simlinked to
~/.config/nvim/lua/custom
