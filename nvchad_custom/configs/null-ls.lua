local null_ls = require("null-ls")

local b = null_ls.builtins

local sources = {
	-- webdev stuff
	b.formatting.prettier.with({
		filetypes = {
			"html",
			"markdown",
			"css",
			"scss",
			"javascript",
			"javascriptreact",
			"javascript.jsx",
			"typescript",
			"typescriptreact",
			"typescript.tsx",
		},
	}),
	-- Lua
	b.formatting.stylua.with({ filetypes = { "lua" } }),
	-- Go
	b.formatting.gofmt.with({ filetypes = { "go" } }),
	b.formatting.goimports.with({ filetypes = { "go" } }),
}

local augroup_lspformatting = vim.api.nvim_create_augroup("LspFormatting", {})

-- Initialize global toggle variable
vim.g.format_on_save_enabled = true

local format_on_save = function(client, bufnr)
	if client.supports_method("textDocument/formatting") then
		vim.api.nvim_clear_autocmds({ group = augroup_lspformatting, buffer = bufnr })
		vim.api.nvim_create_autocmd("BufWritePre", {
			group = augroup_lspformatting,
			buffer = bufnr,
			callback = function()
				if vim.g.format_on_save_enabled then
					vim.lsp.buf.format({ bufnr = bufnr })
				end
			end,
		})
	end
end

null_ls.setup({
	debug = true,
	sources = sources,
	on_attach = format_on_save,
})

-- Toggle command
vim.api.nvim_create_user_command("ToggleFormatOnSave", function()
	vim.g.format_on_save_enabled = not vim.g.format_on_save_enabled
	print("Format on save " .. (vim.g.format_on_save_enabled and "enabled" or "disabled"))
end, {})
