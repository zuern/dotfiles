---@type MappingsTable
local M = {}

local opts = { nowait = true }
M.general = {
	n = {
		[";"] = { ":", "enter command mode", opts },
		["<C-p>"] = { "<cmd> Telescope live_grep <CR>", "Launch Telescope", opts },
		["<leader>fd"] = { "<cmd> Telescope fd <CR>", "Find files", opts },
		["<leader>fr"] = { "<cmd> Telescope lsp_references <CR>", "Find LSP references", opts },
		["<leader>ft"] = { "<cmd> Telescope <CR>", "Open Telescope", opts },
		["<leader>tf"] = { "<cmd> ToggleFormatOnSave <CR>", "Toggle format on save", opts },
		["<leader>tw"] = { "<cmd> set wrap! <CR>", "Toggle line wrapping", opts },
	},
}

-- more keybinds!

return M
