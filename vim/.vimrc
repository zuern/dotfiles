set encoding=utf-8

" Set hybrid relative line numbering
set number relativenumber

" Spell-check Markdown files and Git Commit Messages
autocmd FileType markdown setlocal spell
autocmd FileType gitcommit setlocal spell

" set Vim-specific sequences for RGB colors
" https://github.com/vim/vim/issues/993
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" Disable relative numbering when in insert mode or in another buffer
" https://jeffkreeftmeijer.com/vim-number/
:augroup numbertoggle
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:  autocmd BufEnter,FocusGained             * set number
:  autocmd BufLeave,FocusLost               * set nonumber
:augroup END

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

" Make vim search case-insensitive if search term is all lowercase
" Else make search case-sensitive
" Also turn on search-as-you-type
set ignorecase
set smartcase
set incsearch

" Disable search highlighting
set nohlsearch

" Make vim splits open to the right and bottom
set splitbelow
set splitright

" Set syn highlighting in python
let python_highlight_all=1
syntax on

" Set hybrid relative line numbering
set number relativenumber

" Plugin management
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'neovim/nvim-lspconfig'                                 " nvim LSP server config
Plug 'williamboman/nvim-lsp-installer'                       " nvim LSP server quick-installer
Plug 'hrsh7th/nvim-compe'                                    " nvim LSP code completion
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " nvim LSP syntax highlighting
Plug 'nvim-lua/popup.nvim'                                   " nvim popup API implementation needed for telescope.
Plug 'nvim-lua/plenary.nvim'                                 " nvim window management needed for popup.
Plug 'nvim-telescope/telescope.nvim'                         " nvim file/text/symbol finder/previewer/picker.
Plug 'kyazdani42/nvim-web-devicons'                          " icons for trouble.nvim
Plug 'folke/trouble.nvim'                                    " code issues list.
Plug 'nathangrigg/vim-beancount'

Plug 'editorconfig/editorconfig-vim'                         " https://editorconfig.org/
Plug 'jparise/vim-graphql'                                   " GraphQL

Plug 'morhetz/gruvbox'                                       " colorscheme.
Plug 'sonph/onehalf', { 'rtp': 'vim' }

Plug 'ayu-theme/ayu-vim'
Plug 'airblade/vim-gitgutter'                                " git in margins
Plug 'chrisbra/csv.vim'                                      " CSV file formatting commands
Plug 'christoomey/vim-tmux-navigator'                        " Navigate vim/tmux seamlessly.
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }           " go support
Plug 'tpope/vim-fugitive'                                    " git
Plug 'vim-airline/vim-airline'                               " fancy ui
call plug#end()

lua << EOF
	require("trouble").setup {}
EOF



" Get vim-airline to use powerline fonts
let g:airline_powerline_fonts = 1

" https://vim.fandom.com/wiki/Indent_with_tabs,_align_with_spaces
set smarttab noexpandtab shiftwidth=2 softtabstop=2 tabstop=2

" Configure indendation for specific file types.
autocmd FileType markdown                setlocal tabstop=2 softtabstop=2 expandtab shiftwidth=2 nosmarttab textwidth=80
autocmd FileType html                    setlocal tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd FileType xml                     setlocal tabstop=2 softtabstop=0 expandtab shiftwidth=2 nosmarttab
autocmd FileType typescript              setlocal tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd FileType cucumber                setlocal tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd FileType typescriptreact         setlocal tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd FileType jinja                   setlocal tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd FileType json                    setlocal tabstop=4 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd FileType javascript              setlocal tabstop=4 softtabstop=0 expandtab shiftwidth=2 smarttab
autocmd BufRead,BufNewFile *.yaml.gotmpl setlocal syntax=yaml tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd FileType java                    setlocal tabstop=2 softtabstop=2 expandtab shiftwidth=2 nosmarttab

" Allow comments in JSON
autocmd FileType json syntax match Comment +\/\/.\+$+

" Delete trailing whitespace automatically.
autocmd BufWritePre * %s/\s\+$//e

" Set vim-go plugin to run goimports on save of *.go files instead of gofmt
" This will format and update imports all at once
let g:go_fmt_command = "goimports"

" Prevent ^G from prefixing folders in NERDTree
" https://stackoverflow.com/a/53708512
let g:NERDTreeNodeDelimiter = "\u00a0"

" Run set list to print whitespace chars with the fancy chars below
set nolist
set listchars=tab:→\ ,space:·,nbsp:␣,trail:█,eol:¶,precedes:«,extends:»

" Preserve undo history on close.
set undodir=~/.vim/undodir
set undofile

" Reduce update time to make gitgutter update more quickly.
set updatetime=100 " ms

" true colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

set termguicolors
let ayucolor="light" " light/dark/mirage
colorscheme ayu          " light theme
colorscheme gruvbox      " dark theme
colorscheme onehalflight " light theme
