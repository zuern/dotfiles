FROM alpine
RUN apk add git zsh neovim coreutils && rm -rf /var/cache/apk/*
ADD . /root/.dotfiles
RUN cd /root && ln -s /root/.dotfiles/zsh/.zshrc && ln -s /root/.dotfiles/zsh/.aliases && ln -s /root/.dotfiles/zsh/.profile
RUN sed -i -e "s/bin\/ash/bin\/zsh/" /etc/passwd
RUN zsh ./root/.zshrc
ENV SHELL /bin/zsh
WORKDIR /root
ENTRYPOINT ["zsh"]
