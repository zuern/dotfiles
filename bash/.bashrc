#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls -h --color=auto'
alias ll='ls -h --color=auto -al'
alias ..='cd ..'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# do not delete / or prompt if deleting more than 3 files at a time #
alias rm='rm -i --preserve-root'
######
# Default Prompt
######

PS1='[\u@\h \W]\$ '

#####
# TheFuck configuration
#####
#eval $(thefuck --alias)
#eval $(thefuck --alias fuck)


######
# Display Git Branch in Prompt
######


function parse_git_branch {
	# Pipes the result of git branch to a stream editor and returns nothing if not a repo or the colored branch name
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1) /'
}

function proml {
	local     DEFAULT="\[\033[0m\]"

	local        BLUE="\[\033[0;34m\]"
	local         RED="\[\033[0;31m\]"
	local   LIGHT_RED="\[\033[1;31m\]"
	local       GREEN="\[\033[0;32m\]"
	local LIGHT_GREEN="\[\033[1;32m\]"
	local       WHITE="\[\033[1;37m\]"
	local  LIGHT_GRAY="\[\033[0;37m\]"

	# Sets the PS1 prompt to conditionally display the current git branch if in a git repo
	PS1="$DEFAULT[\u@\h:\$PWD] $GREEN\$(parse_git_branch)$DEFAULT\$ "
}

proml

source ~/.profile

complete -C /home/kevin/.local/bin/mc mc
