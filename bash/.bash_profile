#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
. ~/.profile

# Start sway on login
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
	exec sway
fi
