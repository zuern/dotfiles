# Dotfiles
This repo contains my configuration for various command line tools

## Usage
If you want to use these dotfiles, you can just symlink them into their corresponding directories.

The _much_ easier way however, is to use [GNU Stow](https://www.gnu.org/software/stow/). Simply install `stow` with your package manager and then follow these steps:

### Automated installation with GNU Stow

Clone the repo
```
git clone https://gitlab.com/zuern/dotfiles ~/.dotfiles
```

Navigate to the dotfiles folder and then install vim config files
```
cd ~/.dotfiles
stow vim
```

You're done!

To uninstall the vim dotfiles:
```
cd ~/.dotfiles
stow -D vim
```
