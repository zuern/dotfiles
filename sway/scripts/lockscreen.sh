#!/bin/bash
swaylock -f \
	--image "${1}" \
	--scaling fit \
	--ring-color 333333 \
	--inside-color 00000070 \
	--key-hl-color FF000070 \
	--debug
