" Installs desired language servers and tree sitter stuff.
:LspInstall bash
:LspInstall css
:LspInstall dockerfile
:LspInstall go
:LspInstall graphql
:LspInstall html
:LspInstall java
:LspInstall json
:LspInstall python
:LspInstall tailwindcss
:LspInstall typescript
:TSInstall all
