alias vim="nvim"
alias zshrc="vim ~/.zshrc"
alias vimrc="vim ~/.vimrc"
alias zconfig='source ~/.zshrc'

# common directories
alias dots='cd ~/.dotfiles'
alias src="cd ~/source"
alias infra="cd ~/source/infrastructure"

# Git stuff
alias gs="git status"
alias gds="git diff --staged"
alias gch="git checkout"
alias gl="git log --date=local"
alias gitupdateall="find . -mindepth 1 -maxdepth 1 -type d -print -exec git -C {} fetch \;"

alias cls="clear"
alias diff="diff --color"

# Docker stuff
alias k="kubectl"
alias dc="docker container"
alias di="docker image"
alias dcrmf="docker container ps -q | xargs docker container rm -f"
alias dirmf="docker image ls -q | xargs docker image rm -f"
alias dvrmf="docker volume ls -q | xargs docker volume rm -f"
alias dcp="docker-compose"
alias devenv=". ~/scripts/dev-env.sh"

# Misc
alias weather="curl wttr.in/Toronto"
alias clock="tty-clock -sc -C 1"
alias :w="echo This isn\'t Vim, dummy! | cowsay"
alias :wq=":w"
alias :q=":w"
alias :qa=":w"
alias :wq=":w"
function screenshot {
	name="~/Pictures/Screenshots/Screenshot_$(date --iso-8601=minutes).png"
	grim $name && echo Saved screenshot to $name
}

#
# Golang development
#
alias metalint="golangci-lint run ./..."
alias gosrc="cd $GOPATH/src"
alias gokev="cd $GOPATH/src/gitlab.com/zuern"

#
# Text colorization
#
grc -h >/dev/null && alias go="grc go"

function watchbuild {
	ack -ft go | entr -c go build ./...
}

# watchtest -v ./... -failfast ...etc
function watchtest {
	find . -name "*.go" | entr -cs "clear && go test $1 $2 $3"
}

# Compute code coverage of the specified package, display in chromium, cleanup
# coverage files when done.
function gocoverage {
	go test $1 -p 1 -coverprofile /tmp/cover.out && \
	go tool cover -html=/tmp/cover.out -o /tmp/cover.html && \
	chromium /tmp/cover.html >/dev/null 2>&1 && \
	rm /tmp/cover.html && rm /tmp/cover.out
}

#
# Handy functions
#

# Fetches the SSL certificate from the specified endpoint (only provide domain/IP and port)
function fetchcert {
	openssl s_client -showcerts -connect $1 </dev/null
}

# Configure fff to cd to directory on exit
f() {
    fff "$@"
    cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/fff/.fff_d")"
}

# Read stdin and send as a GraphQL document to localhost:3000.
function graphqlRequest {
	curl -sS -w "\n" "http://localhost:3000/graphql?indent=2" -H "Content-Type: application/graphql" -d @-
}

# Find each go file in the current dir and add a copyright statement above the package declaration
function addcopyrights {
	find . -name "*.go" | while read -r file; do
		sed -i "1 s/package/\/\/ Copyright $(date +%Y) $1. All rights reserved.\n\npackage/" $file && echo $file
	done
}
