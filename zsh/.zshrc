# Start SSH Agent if not already running, otherwise attach to existing socket.
# https://www.electricmonk.nl/log/2012/04/24/re-use-existing-ssh-agent-cygwin-et-al/
local run_ssh_agent=false
for host in "6c7e67ce3258", "Pro"; do
	if [ "$(hostname)" = "$host" ]; then
		run_ssh_agent=true
		break
	fi
done
if $run_ssh_agent; then
	export SSH_AUTH_SOCK=~/.ssh-auth-socket
	ssh-add -l >/dev/null 2>&1
	local code=$?
	if [ $code = 0 ]; then
		echo SSH Agent is running.
	elif [ $code = 2 ]; then
		echo Starting SSH agent.
		rm -rf "$SSH_AUTH_SOCK"
		ssh-agent -a "$SSH_AUTH_SOCK" >| /tmp/.ssh-script
		source /tmp/.ssh-script
		echo $SSH_AGENT_PID >| ~/.ssh-agent-pid
		rm /tmp/.ssh-script
		ssh-add ~/.ssh/id_ecdsa >/dev/null
	fi
fi


# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Download zsh theme if needed.
[[ -d ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k ]] || git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Path to oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

#
# Start sway on login
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
	exec sway
fi

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
	source /etc/profile.d/vte.sh
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#
##
### POWERLEVEL10K CONFIGURATION
##
#

# Export username so Powerlevel9K knows my 'typical' user for the context plugin
DEFAULT_USER="$USER"

# Configure the linux icon for os_icon prompt element
# POWERLEVEL9K_LINUX_ICON='\uF303'
# Set font mode to nerdfont
# Requires terminal font to be from https://github.com/ryanoasis/nerd-fonts
POWERLEVEL10K_MODE='nerdfont-complete'

ZSH_THEME="powerlevel10k/powerlevel10k"

# Fix cut off glpyhs in powerline
# https://github.com/bhilburn/powerlevel9k/issues/231
#POWERLEVEL9K_HOME_ICON=$'\UE12C '

# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(ssh os_icon root_indicator context dir dir_writable vcs newline prompt_char)
# POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status background_jobs history battery time)

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# zsh-autosuggestions:	https://github.com/zsh-users/zsh-autosuggestions
if [ ! -d ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions ]; then
	echo Downloading zsh-autosuggestions...
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
fi
plugins=(git colored-man-pages vi-mode virtualenv zsh-autosuggestions)


source $ZSH/oh-my-zsh.sh


if go version >/dev/null 2>&1; then
	export GOPRIVATE=gitlab.com/zuern
	export GO111MODULE="on"
	export GOPATH="${HOME}/source/go-projects"
	export PATH=$GOPATH/bin:$PATH
fi

complete -o nospace -C /home/$USER/.local/bin/mc mc
if [ -f ~/.dotfiles/zsh/.amazon-env ]; then
	source ~/.dotfiles/zsh/.amazon-env
fi

#
##
### MY ENVIRONMENT
##
#
export BROWSER="firefox"
export TERM="xterm-256color"
export VAULT_ADDR="https://vault.zuern.ca"

export LANG=en_US.UTF-8
export EDITOR='nvim'
export PATH=$PATH:~/.local/bin
export PATH=$PATH:~/.emacs.d/bin
export GDK_SCALE=1

if [[ hostname == "xps" ]]; then
	# Make firefox run natively on wayland
	export MOZ_ENABLE_WAYLAND=1
fi

# make delete key actually delete things
bindkey "^[[3~" delete-char

# Load aliases
if [ -f ~/.dotfiles/zsh/.aliases ]; then
    . ~/.dotfiles/zsh/.aliases
fi

# fzf configuration
fzf -h >/dev/null 2>&1
if [ $? -eq 0 ]; then
  source <(fzf --zsh)
	export FZF_DEFAULT_COMMAND='rg --files'
	export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
fi

zoxide -h >/dev/null 2>&1
if [ $? -eq 0 ]; then
  eval "$(zoxide init zsh)"
  alias cd="z"
fi

source ~/.dotfiles/zsh/.profile
