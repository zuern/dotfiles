export PATH=/home/kevin/scripts:$PATH
export ANSIBLE_NOCOWS=1

# Find git diff-highlight script and add to path.
brew --prefix git 1>/dev/null 2>&1 && export PATH=$(brew --prefix git)/share/git-core/contrib/diff-highlight/:$PATH
[ -f /usr/share/git-core/contrib/diff-highlight ] && export PATH=/usr/share/git-core/contrib/:$PATH
[ -f /usr/share/git-core/contrib/diff-highlight/diff-highlight ] && export PATH=/usr/share/git-core/contrib/diff-highlight:$PATH
[ -f /usr/share/git/diff-highlight/diff-highlight ] && export PATH=/usr/share/git/diff-highlight:$PATH

# https://wiki.archlinux.org/index.php/Dolphin#Icons_not_showing
[ "$XDG_CURRENT_DESKTOP" = "KDE" ] || [ "$XDG_CURRENT_DESKTOP" = "GNOME" ] || export QT_QPA_PLATFORMTHEME="qt5ct"

export TZ='America/Toronto'
